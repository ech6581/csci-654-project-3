/*
 * BreakingRSA.java
 *
 * Heavily influenced and based upon the overall structure of JCudaVectorAdd from
 * parallelcomputing/cuda_examples (P. Hu, 2019)
 *
 * Eric Hartman (ech6581@rit.edu)
 * 11/2/2019
 */

// Lots of imports required by jcuda...
import static jcuda.driver.JCudaDriver.cuCtxCreate;
import static jcuda.driver.JCudaDriver.cuCtxSynchronize;
import static jcuda.driver.JCudaDriver.cuDeviceGet;
import static jcuda.driver.JCudaDriver.cuInit;
import static jcuda.driver.JCudaDriver.cuLaunchKernel;
import static jcuda.driver.JCudaDriver.cuMemAlloc;
import static jcuda.driver.JCudaDriver.cuMemFree;
import static jcuda.driver.JCudaDriver.cuMemcpyDtoH;
import static jcuda.driver.JCudaDriver.cuMemcpyHtoD;
import static jcuda.driver.JCudaDriver.cuModuleGetFunction;
import static jcuda.driver.JCudaDriver.cuModuleLoad;

import jcuda.Pointer;
import jcuda.Sizeof;
import jcuda.driver.CUcontext;
import jcuda.driver.CUdevice;
import jcuda.driver.CUdeviceptr;
import jcuda.driver.CUfunction;
import jcuda.driver.CUmodule;
import jcuda.driver.JCudaDriver;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * BreakingRSA
 *
 * Class that uses brute force to decipher a numeric cipher text
 * by finding 0 to 3 cubic roots of the form c = m^3 (mod n),
 * where m is the encrypted message, n is the modulus, and
 * c is the cipher text.
 *
 * Dependencies:
 *   nvidia gpu
 *   BreakingRSA.cu (kernel function source code)
 *   BreakingRSA.ptx (for execution)
 */
public class BreakingRSA
{
    /**
     * Entry point for the program
     *
     * @param args = cipherText, modulus
     * @throws IOException If an IO error occurs
     */
    public static void main(String args[]) throws IOException
    {
        // Simple usage enforcement
        if(args.length != 2) {
            System.out.println("Usage: <cipherText> <modulus>");
            System.exit(0);
        }

        // Parse parameter inputs...
        long cipherText = Long.parseLong(args[0]);
        long modulus = Long.parseLong(args[1]);

        // Enable exceptions and omit all subsequent error checks
        JCudaDriver.setExceptionsEnabled(true);

        // Initialize the driver and create a context for the first device.
        cuInit(0);
        CUdevice device = new CUdevice();
        cuDeviceGet(device, 0);
        CUcontext context = new CUcontext();
        cuCtxCreate(context, 0, device);

        // Load the ptx file.
        CUmodule module = new CUmodule();
        cuModuleLoad(module, "BreakingRSA.ptx");

        // Obtain a function pointer to the "rsa" function.
        CUfunction function = new CUfunction();
        cuModuleGetFunction(function, module, "rsa");

        // Allocate and fill mIndex
        int mIndexSize = 1;
        int hostMIndex[] = new int[mIndexSize];
        hostMIndex[0] = 0;

        // Allocate the device mIndex, and copy the host mIndex data to the device
        CUdeviceptr deviceMIndex = new CUdeviceptr();
        cuMemAlloc(deviceMIndex, mIndexSize * Sizeof.INT);
        cuMemcpyHtoD(deviceMIndex, Pointer.to(hostMIndex), mIndexSize * Sizeof.INT);

        // Allocate and fill mArray
        int mArraySize = 3;

        // Allocate the device mArray, and copy the host mArray data to the device
        CUdeviceptr deviceMArray = new CUdeviceptr();
        cuMemAlloc(deviceMArray, mArraySize * Sizeof.LONG);

        // Set up the kernel parameters: A pointer to an array
        // of pointers which point to the actual values.
        // This must align with the "rsa" function in BreakingRSA.cu
        Pointer kernelParameters = Pointer.to(
                Pointer.to(new long[]{cipherText}),
                Pointer.to(new long[]{modulus}),
                Pointer.to(deviceMIndex),
                Pointer.to(deviceMArray)
        );

        // Call the kernel function.
        int threadUnits = 1024;
        int blockSizeX = 256;
        int gridSizeX = (int)Math.ceil((double)threadUnits / blockSizeX);
        cuLaunchKernel(function,
                gridSizeX,  1, 1,      // Grid dimension
                blockSizeX, 1, 1,      // Block dimension
                0, null,               // Shared memory size and stream
                kernelParameters, null // Kernel- and extra parameters
        );
        cuCtxSynchronize();

        // Allocate host mArray memory and copy from the device mArray
        long hostMArray[] = new long[mArraySize];
        cuMemcpyDtoH(Pointer.to(hostMArray), deviceMArray,
                mArraySize * Sizeof.LONG);

        // Let's build a List of sorted cubic roots...
        List<Long> hostMList = new ArrayList<Long>();
        for(int i = 0; i < mArraySize && hostMArray[i] != 0; i++) {
            hostMList.add(hostMArray[i]);
        }
        Collections.sort(hostMList);

        // Let's print output...
        if(hostMList.size() == 9) {
            System.out.println("No cube roots of " + cipherText + " (mod " + modulus + ")");
        } else {
            // Print out the cubic roots...
            for (Long m : hostMList) {
                System.out.println("" + m + "^3 = " + cipherText + " (mod " + modulus + ")");
            }
        }

        // Clean up.
        cuMemFree(deviceMArray);
        cuMemFree(deviceMIndex);
    }
}