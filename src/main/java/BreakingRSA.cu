///
// BreakingRSA.cu
//
//   CUDA kernel module containing rsa function.
//
//   Eric Hartman (ech6581@rit.edu)
//   11/2/2019
///


///
// function rsa
//
//    CUDA kernel function  that uses brute force to find cubic roots
//    of a cipher text to decrypt the underlying numeric message.
//    Finds up to 3 cubic roots and returns them in mArray[].
//
//    @param cipher  - an encrypted numeric message
//    @param modulus - a modulus
//    @param mIndex  - atomically updated position of our mArray stack containing cubic roots
//    @param mArray  - stack containing cubic roots, indexed by mIndex
///
extern "C"
__global__ void rsa(long cipher, long modulus, int *mIndex, long *mArray)
{
    // Calculate our stride...
    int start = blockIdx.x * blockDim.x + threadIdx.x;
    int stride = blockDim.x * gridDim.x;

    // Brute force search all possible m's for a cubic root...
    long c = 0;
    for(long i = start; i <= (modulus-1); i += stride) {
        // Calculate the cubic root for this modulus
        c = ((i * i % modulus) * i ) % modulus;

        // Check if found a cubic root!
        if(c == cipher) {
           // Store cubic root in next position of our modulus array result...
           mArray[atomicAdd(mIndex, 1)] = i;
        }
    }
}