## CSCI-654 Project 3
This project implements a simple RSA brute force decryption mechanism.

### Compile JAR
To compile the jar archive, use Maven's Lifecycle -> package option.

### Compile BreakingRSA.cu file
To compile the BreakingRSA.cu file, use the following:
```
cd csci-654-project-3/src/main/java
nvcc -ptx BreakingRSA.cu -o BreakingRSA.ptx
```

### Deployment
To deploy the program, use the following:
```
cd csci-654-project-3/src/main/
scp *.ptx kraken.cs.rit.edu:~/.
scp ../../../target/cuda_examples-1.0-SNAPSHOT.jar kraken.cs.rit.edu:~/.
```
### Running BreakingRSA
To run the program, use the following:
```
ssh kraken.cs.rit.edu
java -cp cuda_examples-1.0-SNAPSHOT.jar BreakingRSA 46054145 124822069
```


    